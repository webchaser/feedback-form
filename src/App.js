import { v4 as uuidv4 } from 'uuid'
import { useState } from 'react'
import Header from "./components/Header"
import FeedbackList from "./components/FeedbackList"
import FeedbackData from "./data/FeedbackData"
import FeedbackStats from './components/FeedbackStats';
import Card from "./components/shared/Card"
import FeedbackForm from './components/FeedbackForm';
import { addItem } from './actions'
import { useDispatch } from 'react-redux';

function App() {
  // Global State
  const [feedback, setFeedback] = useState(FeedbackData);
  const dispatch = useDispatch();

  const addFeedback = (newFeedback) => {
    newFeedback.id = uuidv4()
    // setFeedback([newFeedback, ...feedback])
    dispatch(addItem(newFeedback))
  }

  const deleteFeedback = (id) => {
    if(window.confirm('Are you sure you want to delete ?')){
      setFeedback(feedback.filter((item) => item.id !== id))
    }
  }

  return (
    <>
    <Header />
    {/* <FeedbackForm /> */}
    <div className="container">
      <FeedbackForm handleAdd={addFeedback} />
      <FeedbackStats feedback={feedback} />
      <FeedbackList
          feedback={feedback} 
          handleDelete={deleteFeedback} />

    </div>
    </>
  )
}

export default App
