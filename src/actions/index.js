export const ADD_ITEM = "ADD_ITEM";
export const DELETE_ITEM = "DELETE_ITEM";

export const addItem = (data) => {
    console.log(data);
    return {
        type: ADD_ITEM,
        payload: {
            id: new Date().getTime().toString(),
            rating:data.rating,
            text:data.text
            
        }
    }
}

export const deleteItem = () => {
    return {
        type: DELETE_ITEM
    }
}

