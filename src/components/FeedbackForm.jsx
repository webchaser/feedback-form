import { useState } from 'react'
import { useDispatch } from "react-redux"
import {addItem, deleteItem } from "../actions/index"
import Card from './shared/Card'
import Button from './shared/Button'
import RatingSelect from './RatingSelect'

function FeedbackForm( {handleAdd} ) {
  const [text, setText] = useState('')
  const dispatch = useDispatch()
  const [rating, setRating] = useState(10)
  const handleTextChange = (e) => {
    setText(e.target.value)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    const newFeedback = {
      text,
      rating
    }
    handleAdd(newFeedback)
    setText('')
  }

  return (
    <Card>
      <form onSubmit={handleSubmit}>
        <h2>How would you rate your service with us?</h2>
        <RatingSelect select={(rating) => setRating(rating)}/>
        <div className='input-group'>
          <input 
            onChange={handleTextChange}
            type='text'
            placeholder='Write a review'
            value={text}
          />  
          <Button type='submit' onClick={() => handleSubmit()}>Send</Button>
        </div>
      </form>
    </Card>
  )
  }
export default FeedbackForm