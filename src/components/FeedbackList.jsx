import FeedbackItem from './FeedbackItem';
import { useSelector } from "react-redux";


function FeedbackList( {feedback, handleDelete} ) {
  const list = useSelector((state) => state.items.list)
  console.log(list);
  
  if (!feedback || feedback.length === 0){
     return <p>No Feedback yet.</p>
  }

  return (
    <div className="feedback-list">
      {list.map((item) => (
          <FeedbackItem 
              key={item.id} 
              item={item}
              handleDelete={handleDelete}
          />  
        ))}
    </div>
    )  
}

export default FeedbackList
