const initialData = {
    list: []
}

const addItem = (state= initialData , action) => {
    switch(action.type){
        case "ADD_ITEM":
        const { id, text, rating } = action.payload;

        return {
               ...state,
                list: [
                    ...state.list,
                {
                    id,
                    text,
                    rating
                }
            ]
        }
        default: return state;
    }
}

export default addItem;