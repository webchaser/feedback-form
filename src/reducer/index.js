import addItem from "./addItem";
import deleteItem from './deleteItem'

import { combineReducers } from "@reduxjs/toolkit";

const rootReducer = combineReducers({
    items: addItem
})

export default rootReducer;